using System;
using Xunit;

namespace UnitTests
{
    public class UnitTest1
    {
        [Fact]
        public void Sqaure_WorksWithTwoPositiveIntegers()
        {
            var expected = 16;
            var a = 2;
            var b = 2; 
            var actual = FindingSquareOfTwoNumbers(a, b);
            Assert.Equal(expected,actual);
        }
         [Fact]
        public void Square_WorksWithTwoNegativeIntegers()
        {
            var expected = 16;
            var a = -2;
            var b = -2;
            var actual = FindingSquareOfTwoNumbers(a, b);
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(-3)]
        [InlineData(-5)]
        [InlineData(-9)]
        public void IsNegative_WorksWithManyInputs(int value)
        {
            Assert.True(IsNegative(value));
        }


        // for more see https://xunit.github.io/docs/getting-started/netcore/cmdline

        int FindingSquareOfTwoNumbers(int x, int y)
        {
            return (x*x) + (y*y) + (2*x*y);
        }

         bool IsNegative(int value)
        {
            return value  < 0 ;
        }
    }
}
