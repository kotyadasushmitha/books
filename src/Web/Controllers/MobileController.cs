using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class MobileController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Nokia()
        {
            return Content("This displays the content in text format");
           
        }
        public IActionResult Iphone()
        {
            return Json(new { name = "JSON", description = "This displays the content in JSON format"});
        }
        public IActionResult Googlepixel()
        {
            return Content("<html><body><h2> This displays the message in HTML format </body></html>","text/html");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
